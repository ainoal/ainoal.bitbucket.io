// Menu
const menuBtn = document.querySelector(".menu-btn");
const menu = document.querySelector(".menu");
const menuNav = document.querySelector(".menu-nav");
const navItems = document.querySelectorAll(".nav-item");

// Set initial state of menu
let showMenu = false;

// Change menu state when I click menu button
menuBtn.addEventListener("click", toggleMenu);

function toggleMenu() {
    if(!showMenu) {
        menuBtn.classList.add("close");
        menu.classList.add("show");
        menuNav.classList.add("show");
        navItems.forEach(item => item.classList.add("show"));

        // Set menu state
        showMenu = true;

    } else {
        menuBtn.classList.remove("close");
        menu.classList.remove("show");
        menuNav.classList.remove("show");
        navItems.forEach(item => item.classList.remove("show"));

        // Set menu state
        showMenu = false;
    }
}

/*****************************************************************************/
// Carousel
if (document.URL.includes("projects.html")) {
    const carouselContainer = document.querySelector(".carousel-container");
    const carouselTrack = document.querySelector(".carousel-track");
    const carouselSlides = document.querySelectorAll(".carousel-slide");
    const btnRight = document.querySelector(".btn-right");
    const btnLeft = document.querySelector(".btn-left");
    const carouselNav = document.querySelector(".carousel-nav");
    const carouselIndicators = document.querySelectorAll(".carousel-indicator"); // Dots

    const slideSize = carouselSlides[0].getBoundingClientRect();
    const slideWidth = slideSize.width;

    // Set slide position, arrange the slides next to one another
    carouselSlides.forEach((slide, i) => slide.style.left = slideWidth * i + "px");

    // When I click the left arrow, move slides to the left
    btnLeft.addEventListener("click", e => {
        const currentSlide = carouselTrack.querySelector(".current-slide");
        const prevSlide = currentSlide.previousElementSibling;
        const currentIndicator = carouselNav.querySelector(".current-slide");
        const prevIndicator = currentIndicator.previousElementSibling;

        moveSlide(carouselTrack, currentSlide, prevSlide);
        updateIndicators(currentIndicator, prevIndicator);
    });

    // When I click the right arrow, move slides to the right
    btnRight.addEventListener("click", e => {
        const currentSlide = carouselTrack.querySelector(".current-slide");
        const nextSlide = currentSlide.nextElementSibling;
        const prevSlide = currentSlide.previousElementSibling;
        const currentIndicator = carouselNav.querySelector(".current-slide");
        const nextIndicator = currentIndicator.nextElementSibling;

        moveSlide(carouselTrack, currentSlide, nextSlide);
        updateIndicators(currentIndicator, nextIndicator);
    });

    // On clicking the nav indicators, move to the correct slide
    // (Unnecessary repetition, should get rid of it later on)
    carouselIndicators[0].addEventListener("click", e => {
        const currentSlide = carouselTrack.querySelector(".current-slide");
        const newSlide = carouselSlides[0];
        const currentIndicator = carouselNav.querySelector(".current-slide");
        const newIndicator = carouselIndicators[0];

        moveSlide(carouselTrack, currentSlide, newSlide);
        updateIndicators(currentIndicator, newIndicator);
    });

    carouselIndicators[1].addEventListener("click", e => {
        const currentSlide = carouselTrack.querySelector(".current-slide");
        const newSlide = carouselSlides[1];
        const currentIndicator = carouselNav.querySelector(".current-slide");
        const newIndicator = carouselIndicators[1];
        
        moveSlide(carouselTrack, currentSlide, newSlide); 
        updateIndicators(currentIndicator, newIndicator);
    });

    carouselIndicators[2].addEventListener("click", e => {
        const currentSlide = carouselTrack.querySelector(".current-slide");
        const newSlide = carouselSlides[2];
        const currentIndicator = carouselNav.querySelector(".current-slide");
        const newIndicator = carouselIndicators[2];
        
        moveSlide(carouselTrack, currentSlide, newSlide); 
        updateIndicators(currentIndicator, newIndicator);
    });

    // Function for moving to the right slide
    // These functions are used both with arrow buttons and with carousel indicators
    function moveSlide(carouselTrack, currentSlide, newSlide) {
        carouselTrack.style.transform = "translateX(-" + newSlide.style.left + ")";
        currentSlide.classList.remove("current-slide");
        newSlide.classList.add("current-slide");
    }

    // Function for emphasizing the right indicator dot after changing slides
    function updateIndicators(currentIndicator, newIndicator) {
        currentIndicator.classList.remove("current-slide");
        newIndicator.classList.add("current-slide");
    }
}

/*****************************************************************************/
// Languages
// Define language data reload anchors
const dataReload = document.querySelectorAll("[data-reload]");

// Translations
var lang = {
    fi: {
        home: "Koti",
        aboutme: "Tietoa minusta",
        projects: "Projektit",
        contactme: "Ota yhteyttä",

        intro: "Tuotantotalouden opiskelija, koodarinalku ja innokas kielten opiskelija",

        bioheading: "Tietoa minusta",
        biotext: "Olen ohjelmointiin hurahtanut tuotantotalouden opiskelija. Tällä hetkellä olen erityisesti kiinnostunut front end -kehityksestä sekä graafisista käyttöliittymistä. Olen alkanut tutustumaan ohjelmoinnin saloihin vasta hiljattain, mutta intoa uuden oppimiseen löytyy valtavasti. Ohjelmoinnin lisäksi suhtaudun intohimoisesti kielten opiskeluun, joka on eräs rakkaimmista harrastuksistani.",
        webdev: "Verkkokehitys",
        htmlcssjs: "HTML, CSS ja Javascript",
        htmlbasics: "HTML:n ja CSS:n perusteet",
        jsbasics: "Javascriptin perusteet",
        staticpages: "Staattisten nettisivujen toteuttaminen HTML:n CSS:n ja Javascriptin avulla",
        programming: "Ohjelmointi",
        inaddition: "Javascriptin lisäksi osaan vähän seuraavia ohjelmointikieliä:",
        basics: "Perusteet",
        dataanalysis: "Yksinkertainen data-analyysi",
        guis: "Alkeet graafisista käyttöliittymistä Tkinterin avulla",
        thebasics: "Perusteet",
        languages: "Kielet",
        fluent: "Hyvä",
        fin: "Suomi",
        eng: "Englanti",
        swe: "Ruotsi",
        intermediate: "Keskitaso",
        ger: "Saksa",
        nl: "Hollanti",
        basicslang: "Alkeet",
        ru: "Venäjä",
        fr: "Ranska",
        es: "Espanja",

        projectsheading: "Joitakin projektejani",
        projectsintro: "Valitettavasti täällä ei ole vielä mitään, mutta toivottavasti pidät Barcelonan-mosaiikkikuvistani :)",
    },

    sv: {
        home: "Hem",
        aboutme: "Lite om mig",
        projects: "Projekter",
        contactme: "Ta kontakt",

        intro: "Universitetsstudent inom produktionsekonomi, strävande programmerare och språkentusiast",

        bioheading: "Lite om mig",
        biotext: "Jag är en produktionsekonomistudent, som är jätteintresserad av allt som har någonting att göra med programmering. Just nu är jag mest intresserad av front end -utveckling och grafiska användargränsnitter. Jag har inte programmerat länge, men jag är entusiatik att lära något nytt varje dag. Utöver programmering älskar jag att lära mig främmande språk.",
        webdev: "Webbutveckling",
        htmlcssjs: "HTML, CSS och Javascript",
        htmlbasics: "Grundläggande kunskaper i HTML och CSS",
        jsbasics: "Grundläggande kunskaper i Javascript",
        staticpages: "Statiska webbsidor med HTML, CSS och Javascript", 
        programming: "Programmering",
        inaddition: "Utöver Javascript kan jag lite...",
        basics: "Grundläggande kunskaper",
        dataanalysis: "Lite data-analys",
        guis: "Basiskunskaper av grafiska användargränssnitter med Tkinter",
        thebasics: "Grundläggande kunskaper",
        languages: "Språk",
        fluent: "Flytande/ bra",
        fin: "Finska",
        eng: "Engelska",
        swe: "Svenska",
        intermediate: "Mellanliggande",
        ger: "Tyska",
        nl: "Holländska",
        basicslang: "Grundläggande",
        ru: "Ryska",
        fr: "Franska",
        es: "Spanska",

        projectsheading: "Mina projekter",
        projectsintro: "Det finns inte någonting här ännu, men hoppas, att du gillar mina foton från Park Güell, Barcelona :)",
    },

    de: {
        home: "Home",
        aboutme: "Über mich",
        projects: "Projekten",
        contactme: "Kontakt",

        intro: "Universitätsstudentin des Wirtschaftsingenieurwesens, zukünftige Programmiererin und Sprachenliebhaberin",

        bioheading: "Über mich",
        biotext: "Ich bin Wirtschaftsingenieurwesenstudentin. Ich interessiere mich für alles, das mit Programmierung zu tun hat. Momentan interessiere ich mich besonders für Front end -Entwicklung und grafische Benutzeroberfläche. Ich programmiere nur seit einem Jahr, aber ich bin jeden Tag bereit, etwas Neues zu lernen.",
        webdev: "Webentwicklung",
        htmlcssjs: "HTML, CSS und Javascript",
        htmlbasics: "Die Grundlagen von HTML und CSS",
        jsbasics: "Die Grundlagen von Javascript",
        staticpages: "Statische Webseiten",
        programming: "Programmierung",
        inaddition: "Außerdem kann ich ein bisschen...",
        basics: "Die Grundlagen",
        dataanalysis: "Ein bisschen Data-analysis",
        guis: "Die Grundlagen von grafischen Benutzeroberflächen mit Tkinter",
        thebasics: "Grundkentnisse",
        languages: "Sprachen",
        fluent: "Fließend",
        fin: "Finnisch",
        eng: "Englisch",
        swe: "Schwedisch",
        intermediate: "Gut",
        ger: "Deutsch",
        nl: "Niederländisch",
        basicslang: "Grundkentnisse",
        ru: "Russisch",
        fr: "Französisch",
        es: "Spanisch",

        projectsheading: "Meine Projekten",
        projectsintro: "Leider gibt es noch nichts hier, aber hoffentlich magen Sie meine Fotos aus Park Güell, Barcelona :)",
    }
};

// Use the translated text on the page
if (window.location.hash) {
    console.log(localStorage.lang);
    // Define language via window hash
    if (window.location.hash === "#fi") {
        home.textContent = lang.fi.home;
        aboutme.textContent = lang.fi.aboutme;
        projects.textContent = lang.fi.projects;
        contactme.textContent = lang.fi.contactme;

        // Define what page you are on and translate accordingly
        if (document.URL.includes("index.html")) {
            intro.textContent = lang.fi.intro;
        } 

        else if (document.URL.includes("about.html")) {
            bioheading.textContent = lang.fi.bioheading;
            biotext.textContent = lang.fi.biotext;
            webdev.textContent = lang.fi.webdev;
            htmlcssjs.textContent = lang.fi.htmlcssjs;
            htmlbasics.textContent = lang.fi.htmlbasics;
            jsbasics.textContent =lang.fi.jsbasics;
            staticpages.textContent = lang.fi.staticpages;
            programming.textContent = lang.fi.programming;
            inaddition.textContent = lang.fi.inaddition;
            basics.textContent = lang.fi.basics;
            dataanalysis.textContent = lang.fi.dataanalysis;
            guis.textContent = lang.fi.guis;
            thebasics.textContent = lang.fi.thebasics;
            languages.textContent = lang.fi.languages;
            fluent.textContent = lang.fi.fluent;
            fin.textContent = lang.fi.fin;
            eng.textContent = lang.fi.eng;
            swe.textContent = lang.fi.swe;
            intermediate.textContent = lang.fi.intermediate;
            ger.textContent = lang.fi.ger;
            nl.textContent = lang.fi.nl;
            basicslang.textContent = lang.fi.basicslang;
            ru.textContent = lang.fi.ru;
            fr.textContent = lang.fi.fr;
            es.textContent = lang.fi.es;
        }

        else if (document.URL.includes("projects.html")) {
            projectsheading.textContent = lang.fi.projectsheading;
            projectsintro.textContent = lang.fi.projectsintro;
        }

        else if (document.URL.includes("contact.html")) {
        }
    }

    else if (window.location.hash === "#sv") {
        home.textContent = lang.sv.home;
        aboutme.textContent = lang.sv.aboutme;
        projects.textContent = lang.sv.projects;
        contactme.textContent = lang.sv.contactme;

        if (document.URL.includes("index.html")) {
            intro.textContent = lang.sv.intro;
        } 

        else if (document.URL.includes("about.html")) {
            bioheading.textContent = lang.sv.bioheading;
            biotext.textContent = lang.sv.biotext;
            webdev.textContent = lang.sv.webdev;
            htmlcssjs.textContent = lang.sv.htmlcssjs;
            htmlbasics.textContent = lang.sv.htmlbasics;
            jsbasics.textContent =lang.sv.jsbasics;
            staticpages.textContent = lang.sv.staticpages;
            programming.textContent = lang.sv.programming;
            inaddition.textContent = lang.sv.inaddition;
            basics.textContent = lang.sv.basics;
            dataanalysis.textContent = lang.sv.dataanalysis;
            guis.textContent = lang.sv.guis;
            thebasics.textContent = lang.sv.thebasics;
            languages.textContent = lang.sv.languages;
            fluent.textContent = lang.sv.fluent;
            fin.textContent = lang.sv.fin;
            eng.textContent = lang.sv.eng;
            swe.textContent = lang.sv.swe;
            intermediate.textContent = lang.sv.intermediate;
            ger.textContent = lang.sv.ger;
            nl.textContent = lang.sv.nl;
            basicslang.textContent = lang.sv.basicslang;
            ru.textContent = lang.sv.ru;
            fr.textContent = lang.sv.fr;
            es.textContent = lang.sv.es;
        }

        else if (document.URL.includes("projects.html")) {
            projectsheading.textContent = lang.sv.projectsheading;
            projectsintro.textContent = lang.sv.projectsintro;
        }

        else if (document.URL.includes("contact.html")) {
        }
    } 

    else if (window.location.hash === "#de") {
        home.textContent = lang.de.home;
        aboutme.textContent = lang.de.aboutme;
        projects.textContent = lang.de.projects;
        contactme.textContent = lang.de.contactme;

        if (document.URL.includes("index.html")) {
            intro.textContent = lang.de.intro;
        } 

        else if (document.URL.includes("about.html")) {
            bioheading.textContent = lang.de.bioheading;
            biotext.textContent = lang.de.biotext;
            webdev.textContent = lang.de.webdev;
            htmlcssjs.textContent = lang.de.htmlcssjs;
            htmlbasics.textContent = lang.de.htmlbasics;
            jsbasics.textContent =lang.de.jsbasics;
            staticpages.textContent = lang.de.staticpages;
            programming.textContent = lang.de.programming;
            inaddition.textContent = lang.de.inaddition;
            basics.textContent = lang.de.basics;
            dataanalysis.textContent = lang.de.dataanalysis;
            guis.textContent = lang.de.guis;
            thebasics.textContent = lang.sv.thebasics;
            languages.textContent = lang.de.languages;
            fluent.textContent = lang.de.fluent;
            fin.textContent = lang.de.fin;
            eng.textContent = lang.de.eng;
            swe.textContent = lang.de.swe;
            intermediate.textContent = lang.de.intermediate;
            ger.textContent = lang.de.ger;
            nl.textContent = lang.de.nl;
            basicslang.textContent = lang.de.basicslang;
            ru.textContent = lang.de.ru;
            fr.textContent = lang.de.fr;
            es.textContent = lang.de.es;
        }

        else if (document.URL.includes("projects.html")) {
            projectsheading.textContent = lang.de.projectsheading;
            projectsintro.textContent = lang.de.projectsintro;
        }

        else if (document.URL.includes("contact.html")) {
        }
    }
    console.log(localStorage.lang);
}

//Reload page (in the chosen language)
for (i = 0; i <= 2; i++) {
    dataReload[i].addEventListener("click", reloadPage);
}

function reloadPage (){
    location.reload(true);
}